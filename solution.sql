=======================================
	MYSQL- CULMINATING ACTIVITY
=======================================

-- 1. Return the customerName of the customers who are from the Philippines

SELECT customerName FROM customers WHERE country = "Philippines";


-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"

SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";


-- 3. Return the product name and MSRP of the product named "The Titanic"

SELECT productName, MSRP FROM products WHERE productName = "The Titanic";


-- 4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"

SELECT lastName, firstName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";


-- 5. Return the names of customers who have no registered state

SELECT customerName FROM customers WHERE state IS NULL;


-- 6. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve

SELECT lastName, firstName, email FROM employees WHERE lastName = "Patterson" && firstName = "Steve";


-- 7. Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000

SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" && creditLimit > 3000;


-- 8. Return the customer numbers of orders whose comments contain the string 'DHL'

SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";


-- 9. Return the product lines whose text description mentions the phrase 'state of the art'

SELECT productLine FROM productLines WHERE textDescription LIKE "%state of the art%";

-- 10. Return the countries of customers without duplication

SELECT DISTINCT country FROM customers;

-- 11. Return the statuses of orders without duplication

SELECT DISTINCT status FROM orders;

-- 12. Return the customer names and countries of customers whose country is USA, France, or Canada

SELECT customerName, country FROM customers WHERE country = [ "USA", "France", "Canada" ];     ---------------


-- 13. Return the first name, last name, and office's city of employees whose offices are in Tokyo

SELECT CONCAT(employees.firstName, ' ', employees.lastName) AS  FullName, offices.city FROM employees LEFT JOIN offices ON employees.officeCode WHERE offices.city = "Tokyo";   -----------------

-- 14. Return the customer names of customers who were served by the employee named "Leslie Thompson"

SELECT customerName FROM customers WHERE salesRepEmployeeNumber = 1166;


-- 15. Return the product name and customer name of products ordered by "Baane Mini Imports"

SELECT products.productName, customers.customerName FROM products JOIN customers ON products.productName = customers.customerName WHERE customerName = "Baane Mini Imports";  -----------------

SELECT products.productName, customers.customerName FROM products
	LEFT JOIN albums ON artists.id = albums.artist_id
	LEFT JOIN songs ON albums.id = songs.album_id;

-----------------


	SELECT artists.name, albums.album_title, songs.song_name FROM artists
	LEFT JOIN albums ON artists.id = albums.artist_id
	LEFT JOIN songs ON albums.id = songs.album_id;



-- 16. Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country

SELECT artists.name, albums.album_title, songs.song_name FROM artists
	LEFT JOIN albums ON artists.id = albums.artist_id
	LEFT JOIN songs ON albums.id = songs.album_id;

-----------------

-- 17. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000

SELECT artists.name, albums.album_title, songs.song_name FROM artists
	LEFT JOIN albums ON artists.id = albums.artist_id
	LEFT JOIN songs ON albums.id = songs.album_id;

-----------------

-- 18. Show the customer's name with a phone number containing "+81".

SELECT customerName, phone FROM customers WHERE phone LIKE  "%+81%";


-- 19. Find all customers from US

SELECT * FROM customers WHERE country = "USA";

-- Show the full details of a customer named La Rochelle Gifts.

SELECT * FROM customers WHERE customerName = "La Rochelle Gifts";


=======================================
	MYSQL - STRETCH GOALS
=======================================

-- 1. Return the customer names of customers whose customer names don't have 'a' in them



-- 2. Return the last names and first names of employees being supervised by "Anthony Bow"


-- 3. Return the product name and MSRP of the product with the highest MSRP



-- 4. Return the number of customers in the UK



-- 5. Return the number of products per product line



-- 6. Return the number of customers served by every employee



-- 7. Show the customer's name with the highest credit limit.
